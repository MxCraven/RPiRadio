# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Main.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(407, 375)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.centralwidget.sizePolicy().hasHeightForWidth())
        self.centralwidget.setSizePolicy(sizePolicy)
        self.centralwidget.setMinimumSize(QtCore.QSize(407, 0))
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.listViewPlayer = QtWidgets.QListView(self.centralwidget)
        self.listViewPlayer.setObjectName("listViewPlayer")
        self.horizontalLayout.addWidget(self.listViewPlayer)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.chkPlayerShuffle = QtWidgets.QCheckBox(self.centralwidget)
        self.chkPlayerShuffle.setObjectName("chkPlayerShuffle")
        self.verticalLayout.addWidget(self.chkPlayerShuffle)
        self.btnPlayerPlay = QtWidgets.QPushButton(self.centralwidget)
        self.btnPlayerPlay.setObjectName("btnPlayerPlay")
        self.verticalLayout.addWidget(self.btnPlayerPlay)
        self.btnPlayerClear = QtWidgets.QPushButton(self.centralwidget)
        self.btnPlayerClear.setObjectName("btnPlayerClear")
        self.verticalLayout.addWidget(self.btnPlayerClear)
        self.btnPlayerOpen = QtWidgets.QPushButton(self.centralwidget)
        self.btnPlayerOpen.setObjectName("btnPlayerOpen")
        self.verticalLayout.addWidget(self.btnPlayerOpen)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.gridLayout.addLayout(self.horizontalLayout, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 407, 23))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "RPiRadio"))
        self.chkPlayerShuffle.setText(_translate("MainWindow", "Shuffle"))
        self.btnPlayerPlay.setText(_translate("MainWindow", "Play"))
        self.btnPlayerClear.setText(_translate("MainWindow", "Clear"))
        self.btnPlayerOpen.setText(_translate("MainWindow", "Open Files"))

